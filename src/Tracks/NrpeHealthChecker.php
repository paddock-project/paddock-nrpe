<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\Apps\Nrpe\Tracks;

use BadPixxel\Paddock\Apps\Nrpe\Collector\NrpeStatusCollector;
use BadPixxel\Paddock\Core\Models\Tracks\AbstractTrack;

class NrpeHealthChecker extends AbstractTrack
{
    /**
     * Track Constructor
     */
    public function __construct()
    {
        parent::__construct("nrpe-health-checker");
        //====================================================================//
        // Track Configuration
        $this->enabled = true;
        $this->description = "[NRPE] Check Configuration Health";
        $this->collector = NrpeStatusCollector::getCode();

        //====================================================================//
        // Add Rules
        //====================================================================//

        $this->addRule("config", array("ne" => true, "eq" => "ok"));
    }
}
